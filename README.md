# README #

Some utility files for computational statistics / machine learning. I may reorganise this in the future. Contributions are welcome.

### How do I get set up? ###

* Requires python 3
* Dependencies
json, pandas, numpy, statsmodels, scipy, matplotlib
